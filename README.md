## Flight board
Flight board shows the status of the flights that are scheduled from now until tomorrow.
The search is looking for a flight with a defined flight number. 
Flight number should include airline code as it displayed in 'Flight' column. For example, KL701.
Empty search shows all flights as during initial loading.

## Quick start

1. Clone the repo: git clone https://alena-p@bitbucket.org/alena-p/flight-board.git
2. Install packages by executing 'npm install' in the project folder
3. The project has been written using Angular 7, so make sure that you have Angular CLI preinstalled or execute the next command: npm install -g @angular/cli
4. Run the project by executing 'ng serve' in the project folder
5. Open your browser on http://localhost:4200/