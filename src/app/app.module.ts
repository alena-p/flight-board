import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { PaginatorComponent } from './components/paginator/paginator.component';
import { DatePipe } from '@angular/common';
import { FlightsBoardComponent } from './components/flights-board/flights-board.component';

@NgModule({
  declarations: [
    AppComponent,
    PaginatorComponent,
    FlightsBoardComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent],
  exports: []
})
export class AppModule { }
