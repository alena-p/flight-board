import { Component, OnInit } from '@angular/core';
import { FlightsService } from '../../services/flights.service';
import { FormControl } from '@angular/forms';

export interface FlightsPage {
  pageSize: number;
  pageNumber: number;
  totalPages: number;
  flights: Flight[];
}

export interface Flight {
  flightNumber: string;
  airlineCode: string;
  departureCity: string;
  arrivalCity: string;
  departureTime: Date;
  arrivalTime: Date;
  status: string;
  statusCode: string;
}

enum FlightStatus {
  PARTIALLY_CANCELLED = 'PARTIALLY_CANCELLED',
  CANCELLED = 'CANCELLED',
  ARRIVED = 'ARRIVED',
  LANDED = 'LANDED',
  EARLY_ARRIVAL = 'EARLY_ARRIVAL',
  DELAYED_ARRIVAL = 'DELAYED_ARRIVAL',
  IN_FLIGHT = 'IN_FLIGHT',
  DEPARTED = 'DEPARTED',
  DELAYED_DEPARTURE = 'DELAYED_DEPARTURE',
  EARLY_DEPARTURE = 'EARLY_DEPARTURE',
  NEW_EARLY_DEPARTURE_TIME = 'NEW_EARLY_DEPARTURE_TIME',
  NEW_DEPARTURE_TIME = 'NEW_DEPARTURE_TIME',
  DIVERTED = 'DIVERTED',
  ON_TIME = 'ON_TIME'
}

@Component({
  selector: 'app-flights-board',
  templateUrl: './flights-board.component.html',
  styleUrls: ['./flights-board.component.css']
})
export class FlightsBoardComponent implements OnInit {
  search = new FormControl('');
  flightsPage: FlightsPage;
  readonly pageSize = 20;

  constructor(private flightsService: FlightsService) { }

  ngOnInit() {
    this.getFlightsPage(0);
  }

  getFlightsPage(pageNumber: number) {
    this.flightsService.getFlightsPage(this.pageSize, pageNumber).then((page: FlightsPage) => {
      this.flightsPage = page;
    }, () => this.flightsPage = null);
  }

  selectPage(page: number) {
    if (this.flightsPage && page !== this.flightsPage.pageNumber) {
      this.getFlightsPage(page);
    }
  }

  searchFlight() {
    if (this.search.value.trim()) {
      this.flightsService.getFlightByNumber(this.search.value).then((page: FlightsPage) => {
        this.flightsPage = page;
      }, () => this.flightsPage = null);
    } else {
      this.getFlightsPage(0);
    }
  }

  getFlightStatusClass(status: FlightStatus): string {
    switch (status) {
      case FlightStatus.CANCELLED: {
        return 'status-red';
      }
      case FlightStatus.NEW_EARLY_DEPARTURE_TIME:
      case FlightStatus.NEW_DEPARTURE_TIME: {
        return 'status-yellow';
      }
      case FlightStatus.DELAYED_ARRIVAL:
      case FlightStatus.DELAYED_DEPARTURE: {
        return 'status-orange';
      }
      default: {
        return 'status-green';
      }
    }
  }
}
