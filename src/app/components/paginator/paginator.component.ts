import { Component, OnInit, OnChanges, Input, Output } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.css']
})
export class PaginatorComponent implements OnInit, OnChanges {

  static maxCountDisplayedPages = 5;
  PaginatorComponent = PaginatorComponent;
  @Input() pagesCount = 1;
  @Output() selectedPage: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  pages: Array<number>;

  constructor() { }

  ngOnInit() {
    this.setPaginator(0);
  }

  ngOnChanges() {
    this.setPaginator(0);
    this.selectPage(0);
  }

  setPaginator(newSelectedPage: number) {
    this.pages = new Array();
    const displayedPagesCount = this.pagesCount > PaginatorComponent.maxCountDisplayedPages
                                  ? PaginatorComponent.maxCountDisplayedPages : this.pagesCount;
    const isNext = this.selectedPage.value <= newSelectedPage;
    const startPage = isNext ? newSelectedPage : newSelectedPage - displayedPagesCount + 1;
    const lastPage = isNext ? newSelectedPage + displayedPagesCount : newSelectedPage + 1;
    for (let i = startPage; i >= startPage && i < lastPage && i < this.pagesCount; i++) {
      this.pages.push(i);
    }
  }

  selectPage(pageNumber: number) {
    if (this.shouldShowDiffPages(pageNumber)) {
      this.setPaginator(pageNumber);
    }
    this.selectedPage.next(pageNumber);
  }

  shouldShowDiffPages(selectedPage: number){
    return selectedPage < this.pages[0] || // if 'previous' was clicked for not displayed pages
            // if 'next' was clicked for not displayed pages
            (selectedPage > this.pages[PaginatorComponent.maxCountDisplayedPages - 1] && selectedPage < this.pagesCount)
  }
}
