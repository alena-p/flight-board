import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Flight, FlightsPage } from '../components/flights-board/flights-board.component';
import { DatePipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class FlightsService {

  private options = {
    headers: {
      'Api-Key': 'y3h253ng3emtque4fdph6g7s',
      'accept-language': 'en-GB',
      accept: 'application/hal+json'
    }
  };

  constructor(private httpClient: HttpClient, private datePipe: DatePipe) { }

  getFlightsPage(pageSize: number, pageNumber: number): Promise<FlightsPage> {
    const dateStr = this.datePipe.transform(new Date(), 'yyyy-MM-dd');

    return this.httpClient.get(`https://api.airfranceklm.com/opendata/flightstatus?` +
                                  `startRange=${dateStr}T00:00:00Z&` +
                                  `endRange=${dateStr}T23:59:59Z&` +
                                  `carrierCode=KL&` +
                                  `movementType=D&` +
                                  `timeOriginType=S&` +
                                  `timeType=L&` +
                                  `pageSize=${pageSize}&` +
                                  `pageNumber=${pageNumber}`, this.options)
      .pipe(map((data: any) => {
              const flights = new Array<Flight>();
              const flightsPage: FlightsPage = {
                pageNumber: data.page.pageNumber,
                pageSize: data.page.pageSize,
                totalPages: data.page.totalPages,
                flights
              };
              data.operationalFlights.forEach(el => {
                flights.push(this.createFlight(el));
              });
              return flightsPage;
            }),
            catchError(this.handleError))
      .toPromise();
  }

  getFlightByNumber(flight: string): Promise<FlightsPage> {
    const dateStr = this.datePipe.transform(new Date(), 'yyyyMMdd');
    const flightNumber = flight.substring(2);
    const normFlightNumber = flightNumber.length >= 4 ? flightNumber : '0'.repeat(4 - flightNumber.length).concat(flightNumber);
    const airlineCode = flight.substring(0, 2);

    return this.httpClient.get(`https://api.airfranceklm.com/opendata/flightstatus/${dateStr}+${airlineCode}+${normFlightNumber}`,
                                this.options)
      .pipe(map((data: any) => {
              const flights = [this.createFlight(data)];
              const flightsPage: FlightsPage = {
                pageNumber: 0,
                pageSize: 1,
                totalPages: 1,
                flights
              };
              return flightsPage;
            }),
            catchError(this.handleError))
      .toPromise();
  }

  private createFlight(data): Flight {
    return {
      flightNumber: data.flightNumber,
      airlineCode: data.airline.code,
      departureCity: data.flightLegs[0].departureInformation.airport.city.nameLangTranl,
      arrivalCity: data.flightLegs[data.flightLegs.length - 1].arrivalInformation.airport.city.nameLangTranl,
      departureTime: data.flightLegs[0].departureInformation.times.scheduled,
      arrivalTime: data.flightLegs[0].arrivalInformation.times.scheduled,
      status: data.flightStatusPublicLangTransl,
      statusCode: data.flightStatusPublic
    };
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(`Backend error: ${error.statusText}`);
    }
    return Promise.reject();
  }
}
